package com.mindtree.user.management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.user.management.model.User;
import com.mindtree.user.management.service.UserService;

@Controller
public class UserManagementController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/")
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("user");
		modelAndView.addObject("user", new User());
		modelAndView.addObject("users", userService.getUsers());
		return modelAndView;
	}	

	@GetMapping("/users")
	public ModelAndView getUser() {
		return new ModelAndView("user", "users", userService.getUsers());
	}

	@PostMapping("/users")
	public ModelAndView save(User user) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:/");
		userService.save(user);
		modelAndView.addObject("users", userService.getUsers());
		return modelAndView;
	}

}
