package com.mindtree.user.management.model;

import lombok.Data;

@Data
public class User {
	
	private String firstName;
	private String lastName;
	private String email;

}
