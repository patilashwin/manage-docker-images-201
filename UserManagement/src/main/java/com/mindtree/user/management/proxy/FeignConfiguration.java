package com.mindtree.user.management.proxy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;

@Configuration
public class FeignConfiguration {

	@Bean
	public Logger.Level configureLogLevel() {
		return Logger.Level.FULL;
	}

}
