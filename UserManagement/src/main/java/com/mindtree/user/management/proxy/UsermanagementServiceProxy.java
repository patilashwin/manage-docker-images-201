package com.mindtree.user.management.proxy;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mindtree.user.management.model.User;

@FeignClient(name = "users", url = "http://userservice:8090/users", configuration = FeignConfiguration.class)
public interface UsermanagementServiceProxy {

	@GetMapping
	public List<User> getAllUser();

	@PostMapping
	public User save(@RequestBody User user);
}
