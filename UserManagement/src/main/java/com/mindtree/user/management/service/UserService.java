package com.mindtree.user.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindtree.user.management.model.User;
import com.mindtree.user.management.proxy.UsermanagementServiceProxy;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService {
	
	@Autowired
	UsermanagementServiceProxy proxy;

	public List<User> getUsers() {
		List<User> users = proxy.getAllUser();
		users.forEach(user -> log.info(user.toString()));
		return users;
	}

	public User save(User user) {
		return proxy.save(user);
	}

}
