<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Management</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style type="text/css">
.myform {
	margin-top: 3%;
	margin-left: 10%;
	border: 1px solid black;
	border-collapse: separate;
	border-spacing: 5px;
	border-style: dashed;
	border-width: medium;
	border-color: green;
	width: 80%;
	height: 70%;
}
</style>
</head>

<body>
	<div class="myform">
		<div class="container">
			<form:form action="/users" method="post" modelAttribute="user">
				<div class="form-group">
					<form:label path="firstName">First Name:</form:label>
					<form:input class="form-control" path="firstName"
						placeholder="Enter first name" />
				</div>
				<div class="form-group">
					<form:label path="lastName">Last Name:</form:label>
					<form:input path="lastName" class="form-control"
						placeholder="Enter last name" />
				</div>
				<div class="form-group">
					<form:label path="email">Email:</form:label>
					<form:input path="email" class="form-control"
						placeholder="Enter email" />
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Submit" />
				</div>
			</form:form>

			<hr>

			<br /> <br />
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">First Name</th>
						<th scope="col">Last Name</th>
						<th scope="col">Email</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${not empty users}">
						<c:forEach var="user" items="${users}" varStatus="status">
							<tr>
								<th scope="row">${status.count}</th>
								<td><c:out value="${user.firstName}" /></td>
								<td><c:out value="${user.lastName}" /></td>
								<td><c:out value="${user.email}" /></td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>

		</div>
	</div>

</body>
</html>