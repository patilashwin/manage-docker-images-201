package com.mindtree.usermanagementservice.controller;

import com.mindtree.usermanagementservice.model.User;
import com.mindtree.usermanagementservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> getAllUser() {
        return userService.getAllUser();
    }

    @PostMapping
    public User save(@RequestBody User user) {
        return userService.save(user);
    }

}
